package com.example.animations_bikes;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TableLayout;

public class MainActivity extends AppCompatActivity {
    private ImageView imageView1;
    private ImageView imageView2;
    private ImageView imageView3;
    private ImageView imageView4;
    private ImageView imageView5;
    private ImageView imageView6;
    private ImageView imageView7;

    private RelativeLayout relativeLayout;
    private RelativeLayout relativeLayout2;
    private TableLayout tableLayout;

    ObjectAnimator objectAnimator1;
    ObjectAnimator objectAnimator2;
    ObjectAnimator objectAnimator3;
    ObjectAnimator objectAnimator4;
    ObjectAnimator objectAnimator5;
    ObjectAnimator objectAnimator6;
    ObjectAnimator objectAnimator7;
    ObjectAnimator objectAnimator8;
    ObjectAnimator objectAnimator9;
    ObjectAnimator objectAnimator10;
    ObjectAnimator objectAnimator11;
    ObjectAnimator objectAnimator12;
    ObjectAnimator objectAnimator13;
    ObjectAnimator objectAnimator14;
    ObjectAnimator objectAnimator15;
    ObjectAnimator objectAnimator16;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageView1 = findViewById(R.id.imageView1);
        imageView2 = findViewById(R.id.imageView2);
        imageView3 = findViewById(R.id.imageView3);
        imageView4 = findViewById(R.id.imageView4);
        imageView5 = findViewById(R.id.imageView5);
        imageView6 = findViewById(R.id.imageView6);
        imageView7 = findViewById(R.id.imageView7);
        relativeLayout = findViewById(R.id.textBegin);
        relativeLayout2 = findViewById(R.id.textSuperior);
        tableLayout = findViewById(R.id.tableLayout);

        objectAnimator1 = ObjectAnimator.ofFloat(imageView1, "translationY", 100, -2200);
        objectAnimator1.setDuration(1000);

        objectAnimator2 = ObjectAnimator.ofFloat(imageView2, "translationX", 0, -500);
        objectAnimator2.setDuration(1000);

        objectAnimator3 = ObjectAnimator.ofFloat(relativeLayout, "alpha", 1, 0);
        objectAnimator3.setDuration(1000);

        objectAnimator4 = ObjectAnimator.ofFloat(relativeLayout, "translationY", 0, -900);
        objectAnimator4.setDuration(1000);

        objectAnimator5 = ObjectAnimator.ofFloat(relativeLayout, "translationX", 0, -500);
        objectAnimator5.setDuration(1000);

        objectAnimator6 = ObjectAnimator.ofFloat(relativeLayout2, "alpha", 0, 1);
        objectAnimator6.setDuration(1000);

        objectAnimator7 = ObjectAnimator.ofFloat(relativeLayout2, "rotation", 0, 360);
        objectAnimator7.setDuration(1000);

        objectAnimator8 = ObjectAnimator.ofFloat(relativeLayout2, "translationX", -500, 0);
        objectAnimator8.setDuration(1000);

        objectAnimator9 = ObjectAnimator.ofFloat(imageView3, "translationX", 1000, 0);
        objectAnimator9.setDuration(1000);

        objectAnimator10 = ObjectAnimator.ofFloat(imageView3, "alpha", 0, 1);
        objectAnimator10.setDuration(1000);

        objectAnimator11 = ObjectAnimator.ofFloat(tableLayout, "alpha", 0, 1);
        objectAnimator11.setDuration(1000);

        objectAnimator12 = ObjectAnimator.ofFloat(tableLayout, "translationY", 200, 0);
        objectAnimator12.setDuration(1000);

        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(objectAnimator1, objectAnimator2, objectAnimator3, objectAnimator4, objectAnimator5, objectAnimator9, objectAnimator10);
        animatorSet.setStartDelay(1000);
        animatorSet.start();

        AnimatorSet animatorSet2 = new AnimatorSet();
        animatorSet2.playTogether(objectAnimator6, objectAnimator7, objectAnimator8, objectAnimator11, objectAnimator12);

        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                animatorSet2.start();
            }
        }, 2000);

        imageView4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                objectAnimator13 = ObjectAnimator.ofFloat(imageView4, "rotation", 0, 360);
                objectAnimator13.setDuration(1000);
                objectAnimator13.start();
                new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(MainActivity.this, MainActivity2.class);
                        startActivity(intent);
                    }
                }, 1000);
            }
        });

        imageView5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                objectAnimator14 = ObjectAnimator.ofFloat(imageView5, "rotation", 0, 360);
                objectAnimator14.setDuration(1000);
                objectAnimator14.start();
                new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(MainActivity.this, MainActivity2.class);
                        startActivity(intent);
                    }
                }, 1000);
            }
        });

        imageView6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                objectAnimator15 = ObjectAnimator.ofFloat(imageView6, "rotation", 0, 360);
                objectAnimator15.setDuration(1000);
                objectAnimator15.start();
                new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(MainActivity.this, MainActivity2.class);
                        startActivity(intent);
                    }
                }, 1000);
            }
        });

        imageView7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                objectAnimator16 = ObjectAnimator.ofFloat(imageView7, "rotation", 0, 360);
                objectAnimator16.setDuration(1000);
                objectAnimator16.start();
                new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(MainActivity.this, MainActivity2.class);
                        startActivity(intent);
                    }
                }, 1000);
            }
        });
    }
}